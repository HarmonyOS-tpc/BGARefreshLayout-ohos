/**
 * Copyright 2015 bingoogolapple
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.bingoogolapple.refreshlayout;

import cn.bingoogolapple.refreshlayout.util.Constants;
import ohos.agp.components.Component;
import ohos.app.Context;

/**
 * BgaStickinessRefreshViewHolder.
 */
public class BgaStickinessRefreshViewHolder extends BgarefreshViewHolder {
    private int mRotateImageResId = Constants.INVALID;

    /**
     * BgaStickinessRefreshViewHolder
     *
     * @param context              Context
     * @param isLoadingmoreenabled Boolean
     */
    public BgaStickinessRefreshViewHolder(Context context, boolean isLoadingmoreenabled) {
        super(context, isLoadingmoreenabled);
    }
}