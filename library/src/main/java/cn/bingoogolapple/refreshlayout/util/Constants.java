/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.bingoogolapple.refreshlayout.util;

/**
 * Function description
 * class Constants
 */
public class Constants {
    /**
     * Invalid ID
     */
    public static final int INVALID = -1;

    /**
     * MAGIC_2 ID
     */
    public static final int DISPLAY_HALF_2 = 2;

    /**
     * MAGIC_255 ID
     */
    public static final int RGB_COLOR_255 = 255;

    /**
     * RGB_COLOR ID
     */
    public static final int RGB_COLOR = 0xFF000000;

    /**
     * MAGIC_150 ID
     */
    public static final int HEIGHT_WIDTH_120 = 120;
    /**
     * MAGIC_150 ID
     */
    public static final int HEIGHT_WIDTH_100 = 80;

    /**
     * MAGIC_40 ID
     */
    public static final int TEXT_SIZE_40 = 40;

    /**
     * MAGIC_800 ID
     */
    public static final int CLOSE_TIME_800 = 1500;

    private Constants() {
        /* Do nothing */
    }
}
