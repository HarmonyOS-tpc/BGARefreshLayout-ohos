/**
 * Copyright 2015 bingoogolapple
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.bingoogolapple.refreshlayout.util;

import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.ScrollView;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.ListContainer;
import ohos.agp.utils.Rect;

import java.lang.reflect.Field;

import static ohos.agp.components.Component.VERTICAL;

/**
 * BGARefreshScrollingUtil.
 */
public class BgarefreshScrollingUtil {
    /**
     * BgarefreshScrollingUtil
     */
    public BgarefreshScrollingUtil() {
    }

    /**
     * isScrollViewOrWebViewToTop
     *
     * @param view Component
     * @return boolean
     */
    public static boolean isScrollViewOrWebViewToTop(Component view) {
        return view != null && view.getScrollValue(Component.AXIS_Y) == 0;
    }

    /**
     * isRecycleViewToTop
     *
     * @param recycleView Component
     * @return boolean
     */
    public static boolean isRecycleViewToTop(ListContainer recycleView) {
        if (recycleView != null) {
            BaseItemProvider manager = recycleView.getItemProvider();
            if (manager == null) {
                return true;
            }
            if (manager.getCount() == 0) {
                return true;
            }
            if (recycleView.getOrientation() == VERTICAL) {
                int firstChildTop = 0;
                if (recycleView.getChildCount() > 0) {
                    Component firstChild = recycleView.getComponentAt(0);
                    ComponentContainer.LayoutConfig layoutParams = firstChild.getLayoutConfig();
                    firstChildTop = firstChild.getTop() - layoutParams.getMarginTop() - getRecyclerViewItemTopInset(
                            layoutParams) - recycleView.getPaddingTop();
                }

                if (recycleView.getFirstVisibleItemPosition() < 1 && firstChildTop == 0) {
                    return true;
                }
            }
        }
        return false;
    }


    private static int getRecyclerViewItemTopInset(ListContainer.LayoutConfig layoutParams) {
        try {
            Field field = ListContainer.LayoutConfig.class.getDeclaredField("mDecorInsets");
            field.setAccessible(true);
            // 开发者自定义的滚动监听器
            Rect rect = null;
            if (field.get(layoutParams) instanceof Rect) {
                rect = (Rect) field.get(layoutParams);
            }
            return rect.top;
        } catch (NoSuchFieldException | IllegalAccessException exception) {
            return 0;
        }
    }

    /**
     * isStickyNavLayoutToTop
     *
     * @param scrollView Component
     * @return boolean
     */
    public static boolean isScrollViewToBottom(ScrollView scrollView) {
        if (scrollView != null) {
            int scrollContentHeight = scrollView.getScrollValue(Component.AXIS_Y) + scrollView.getHeight()
                    - scrollView.getPaddingTop() - scrollView.getPaddingBottom();
            int realContentHeight = scrollView.getComponentAt(0).getHeight();
            if (scrollContentHeight == realContentHeight) {
                return true;
            }
        }
        return false;
    }

    /**
     * isRecyclerViewToBottom
     *
     * @param recyclerView ListContainer
     * @return boolean
     */
    public static boolean isRecyclerViewToBottom(ListContainer recyclerView) {
        return false;
    }

    /**
     * scrollToBottom
     *
     * @param scrollView ScrollView
     */
    public static void scrollToBottom(final ScrollView scrollView) {
        if (scrollView != null) {
            scrollView.fluentScrollByY(ScrollView.DRAG_DOWN);
        }
    }

    /**
     * scrollToBottom
     *
     * @param recyclerView ListContainer
     */
    public static void scrollToBottom(final ListContainer recyclerView) {
        if (recyclerView != null) {
            if (recyclerView.getItemProvider() != null && recyclerView.getItemProvider().getCount() > 0) {
                recyclerView.scrollTo(recyclerView.getItemProvider().getCount() - 1);
            }
        }
    }

}