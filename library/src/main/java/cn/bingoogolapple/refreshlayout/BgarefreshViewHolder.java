/**
 * Copyright 2015 bingoogolapple
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.bingoogolapple.refreshlayout;

import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.components.element.FrameAnimationElement;
import ohos.agp.utils.Color;
import ohos.app.Context;

/**
 * BgarefreshViewHolder.
 */
public abstract class BgarefreshViewHolder {
    String loadMoreText = "Loading...";
    String mPullDownRefreshText = "Loading...";
    int mUltimateColorResId = ResourceTable.Color_default_ultimate_color;
    int mLoadMoreBackgroundColorRes = ResourceTable.Color_default_load_more_view_background_color;
    int mLoadMoreBackgroundDrawableRes = -1;
    int mRefreshViewBackgroundDrawableRes = -1;
    int mRefreshViewBackgroundColorRes = ResourceTable.Color_default_refresh_view_background_color;
    int mOriginalImageResId = -1;
    int loaderTextSize = 50;
    int progressSize = 90;
    int loaderTextColor = -1;
    boolean pullUpDilogueVisible = false;
    Component refreshHeaderView = null;
    Component mLoadMoreFooterView = null;
    /**
     * mIsLoadingMoreEnabled
     */
    boolean isLoadingMoreEnabled;
    /**
     * Context
     */
    private Context mContext;

    /**
     * BgarefreshLayout
     */
    private BgarefreshLayout mRefreshLayout;

    /**
     * Text
     */
    private Text mFooterStatusTv;

    /**
     * Image
     */
    private Image mFooterChrysanthemumIv;

    /**
     * FrameAnimationElement
     */
    private FrameAnimationElement mFooterChrysanthemumAd;

    /**
     * mLodingMoreText
     */
    private String mLodingMoreText = "加载中...";

    /**
     * BgarefreshViewHolder
     *
     * @param context              Context
     * @param isLoadingMoreEnabled Boolean
     */
    public BgarefreshViewHolder(Context context, boolean isLoadingMoreEnabled) {
        mContext = context;
        this.isLoadingMoreEnabled = isLoadingMoreEnabled;
    }

    /**
     * getLoadMoreFooterView
     *
     * @return mLoadMoreFooterView value
     */
    public Component getLoadMoreFooterView() {
        if (!isLoadingMoreEnabled) {
            mLoadMoreFooterView = null;
            return mLoadMoreFooterView;
        }
        if (mLoadMoreFooterView != null) {
            return mLoadMoreFooterView;
        } else {
            mLoadMoreFooterView = LayoutScatter.getInstance(mContext)
                    .parse(ResourceTable.Layout_view_normal_refresh_footer, null, false);

            mLoadMoreFooterView.setScrollbarBackgroundColor(Color.TRANSPARENT);
            if (mLoadMoreFooterView.findComponentById(
                    ResourceTable.Id_tv_normal_refresh_footer_status) instanceof Text) {
                mFooterStatusTv = (Text) mLoadMoreFooterView.findComponentById(
                        ResourceTable.Id_tv_normal_refresh_footer_status);
            }
            if (mLoadMoreFooterView.findComponentById(
                    ResourceTable.Id_iv_normal_refresh_footer_chrysanthemum) instanceof Image) {
                mFooterChrysanthemumIv = (Image) mLoadMoreFooterView.findComponentById(
                        ResourceTable.Id_iv_normal_refresh_footer_chrysanthemum);
            }
            if (mFooterChrysanthemumIv.getImageElement() instanceof FrameAnimationElement) {
                mFooterChrysanthemumAd = (FrameAnimationElement) mFooterChrysanthemumIv.getImageElement();
            }
            mFooterStatusTv.setText(mLodingMoreText);
        }
        return mLoadMoreFooterView;
    }

    /**
     * changeToLoadingMore
     */
    public void changeToLoadingMore() {
        if (isLoadingMoreEnabled && mFooterChrysanthemumAd != null) {
            mFooterChrysanthemumAd.start();
        }
    }

    /**
     * setRefreshLayout
     *
     * @param refreshLayout BgarefreshLayout
     */
    public void setRefreshLayout(BgarefreshLayout refreshLayout) {
        mRefreshLayout = refreshLayout;
    }

    /**
     * setPullDownRefreshText
     *
     * @param mPullDownRefreshText String
     */
    public void setPullDownRefreshText(String mPullDownRefreshText) {
        this.mPullDownRefreshText = mPullDownRefreshText;
    }

    /**
     * setLoadingMoreText
     *
     * @param loadMoreText String
     */
    public void setLoadingMoreText(String loadMoreText) {
        this.loadMoreText = loadMoreText;
    }

    /**
     * setUltimateColor
     *
     * @param mUltimateColorResId int
     */
    public void setUltimateColor(int mUltimateColorResId) {
        this.mUltimateColorResId = mUltimateColorResId;
    }

    /**
     * setLoadMoreBackgroundColorRes
     *
     * @param mLoadMoreBackgroundColorRes int
     */
    public void setLoadMoreBackgroundColorRes(int mLoadMoreBackgroundColorRes) {
        this.mLoadMoreBackgroundColorRes = mLoadMoreBackgroundColorRes;
    }

    /**
     * setRefreshViewBackgroundColorRes
     *
     * @param mRefreshViewBackgroundColorRes int
     */
    public void setRefreshViewBackgroundColorRes(int mRefreshViewBackgroundColorRes) {
        this.mRefreshViewBackgroundColorRes = mRefreshViewBackgroundColorRes;
    }

    /**
     * setPullUpDilogueVisible
     *
     * @param pullUpDilogueVisible boolean
     */
    public void setPullUpDilogueVisible(Boolean pullUpDilogueVisible) {
        this.pullUpDilogueVisible = pullUpDilogueVisible;
    }

    /**
     * setOriginalImage
     *
     * @param mOriginalImageResId int
     */
    public void setOriginalImage(int mOriginalImageResId) {
        this.mOriginalImageResId = mOriginalImageResId;
    }

    /**
     * getRefreshHeaderView
     *
     * @return Component
     */
    public Component getRefreshHeaderView() {
        return refreshHeaderView;
    }

    /**
     * setLoaderTextSize
     *
     * @param loaderTextSize int
     */
    public void setLoaderTextSize(int loaderTextSize) {
        this.loaderTextSize = loaderTextSize;
    }

    /**
     * setProgressSize
     *
     * @param progressSize int
     */
    public void setProgressSize(int progressSize) {
        this.progressSize = progressSize;
    }

    /**
     * setLoaderTextColor
     *
     * @param loaderTextColor int
     */
    public void setLoaderTextColor(int loaderTextColor) {
        this.loaderTextColor = loaderTextColor;
    }

    /**
     * setLoadMoreBackgroundDrawableRes
     *
     * @param loadMoreBackgroundDrawableRes image
     */
    public void setLoadMoreBackgroundDrawableRes(int loadMoreBackgroundDrawableRes) {
        mLoadMoreBackgroundDrawableRes = loadMoreBackgroundDrawableRes;
    }

    /**
     * setRefreshViewBackgroundDrawableRes
     *
     * @param refreshViewBackgroundDrawableRes image
     */
    public void setRefreshViewBackgroundDrawableRes(int refreshViewBackgroundDrawableRes) {
        mRefreshViewBackgroundDrawableRes = refreshViewBackgroundDrawableRes;
    }
}