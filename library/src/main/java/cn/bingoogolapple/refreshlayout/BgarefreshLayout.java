/**
 * Copyright 2015 bingoogolapple
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.bingoogolapple.refreshlayout;

import cn.bingoogolapple.refreshlayout.util.BgarefreshScrollingUtil;
import cn.bingoogolapple.refreshlayout.util.Constants;
import cn.bingoogolapple.refreshlayout.util.LogUtil;
import cn.bingoogolapple.refreshlayout.util.Utils;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Image;
import ohos.agp.components.ListContainer;
import ohos.agp.components.ScrollView;
import ohos.agp.components.Text;
import ohos.agp.components.StackLayout;
import ohos.agp.components.ComponentParent;
import ohos.agp.components.DragInfo;
import ohos.agp.components.DragEvent;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.AttrSet;
import ohos.agp.components.RoundProgressBar;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.agp.utils.TextAlignment;
import ohos.agp.window.dialog.CommonDialog;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;

import java.util.Optional;

import static ohos.agp.components.ComponentContainer.LayoutConfig.MATCH_CONTENT;
import static ohos.agp.components.ComponentContainer.LayoutConfig.MATCH_PARENT;
import static ohos.agp.utils.LayoutAlignment.BOTTOM;
import static ohos.agp.utils.LayoutAlignment.CENTER;

/**
 * BgameiTuanRefreshViewHolder.
 */
public class BgarefreshLayout extends DirectionalLayout {
    private BgarefreshViewHolder mRefreshViewHolder;
    private boolean mPullDownRefreshEnable = true;
    private boolean mIsShowLoadingMoreView = true;
    private int listSize = -1;

    /**
     * mWholeHeaderView
     */
    private DirectionalLayout mWholeHeaderView;

    /**
     * 下拉刷新控件下方的自定义控件
     */
    private Component mCustomHeaderView;
    private CommonDialog popupDialog;

    /**
     * mIsCustomHeaderViewScrollable
     */
    private boolean mIsCustomHeaderViewScrollable = false;
    private int topUpdateY = 0;
    private int bottomUpdateY = 0;
    private int endY = 0;
    private int mCount = 1;
    private int startY = 1;
    private Component.DraggedListener mDragListener = null;

    /**
     * mCurrentRefreshStatus
     */
    private RefreshStatus mCurrentRefreshStatus = RefreshStatus.IDLE;

    /**
     * mLoadMoreFooterView
     */
    private Component mLoadMoreFooterView;

    /**
     * mLoadMoreFooterViewHeight
     */
    private int mLoadMoreFooterViewHeight;

    /**
     * BgarefreshLayoutDelegate
     */
    private BgarefreshLayoutDelegate bgarefreshLayoutDelegate;

    /**
     * mIsLoadingMore
     */
    private boolean mIsLoadingMore = false;

    private ScrollView mScrollView;

    private ListContainer mRecyclerView;

    private Component mNormalView;


    private Component mContentView;

    /**
     * 是否已经设置内容控件滚动监听器
     */
    private boolean mIsInitedContentViewScrollListener = false;

    /**
     * 触发上拉加载更多时是否显示加载更多控件
     */
    private DirectionalLayout myLayout;
    private EventHandler mEventHandler;
    private Text refreshText;
    private Image logo;
    private RoundProgressBar refreshRoundProgress;
    private DirectionalLayout myBottomParentLayout;
    private DirectionalLayout myBottomLayout;

    /**
     * BgarefreshLayout
     *
     * @param context Context
     */
    public BgarefreshLayout(Context context) {
        this(context, null);
        if (mDragListener == null) {
            createDragListener();
        }
        super.setDraggedListener(1, mDragListener);
    }

    /**
     * BgarefreshLayout
     *
     * @param context Context
     * @param attrs   Attrset
     */
    public BgarefreshLayout(Context context, AttrSet attrs) {
        super(context, attrs);
        if (mDragListener == null) {
            createDragListener();
        }
        mEventHandler = new EventHandler(EventRunner.getMainEventRunner());
        initWholeHeaderView();
        super.setDraggedListener(1, mDragListener);
    }

    private void createDragListener() {
        Optional<Display> mOptional = DisplayManager.getInstance()
                .getDefaultDisplay(getContext().getApplicationContext());
        Display display = mOptional.get();
        Point mDisplayPoint = new Point();
        display.getSize(mDisplayPoint);
        mDragListener = new Component.DraggedListener() {
            @Override
            public void onDragDown(Component component, DragInfo dragInfo) {
            }

            @Override
            public void onDragStart(Component component, DragInfo dragInfo) {
                startY = Math.round(Math.round(dragInfo.startPoint.position[1]));
            }

            @Override
            public void onDragUpdate(Component component, DragInfo dragInfo) {
                updateCalculation(mDisplayPoint, dragInfo);
                startY = 0;
            }

            @Override
            public void onDragEnd(Component component, DragInfo dragInfo) {
                if (mCurrentRefreshStatus != RefreshStatus.REFRESHING) {
                    mCurrentRefreshStatus = RefreshStatus.RELEASE_REFRESH;
                    Point mPoint = dragInfo.updatePoint;
                    endY = Math.round(mPoint.position[1]);
                    mCount++;
                    if (mPullDownRefreshEnable && topUpdateY != 0 &&
                            Math.round(Math.round(mDisplayPoint.position[1]) / Constants.DISPLAY_HALF_2)
                                    < endY) {
                        mCurrentRefreshStatus = RefreshStatus.REFRESHING;
                        if (component instanceof BgarefreshLayout) {
                            bgarefreshLayoutDelegate.onBgarefreshLayoutBeginRefreshing((BgarefreshLayout) component);
                        }
                    } else if (mIsLoadingMore && bottomUpdateY != 0
                            && Math.round(Math.round(mDisplayPoint.position[1]) / 2) > endY) {
                        bgarefreshLayoutDelegate.onBgarefreshLayoutBeginLoadingMore(BgarefreshLayout.this);
                        showPopupDialog();
                    } else {
                        LogUtil.debug("Info", "Not handeled");
                    }
                    endLoadingMore();
                }
                bottomUpdateY = 0;
                topUpdateY = 0;
                endY = 0;
            }

            @Override
            public void onDragCancel(Component component, DragInfo dragInfo) {
                // Do something
            }
        };
    }

    private void updateCalculation(Point mDisplayPoint, DragInfo dragInfo) {
        if (mCurrentRefreshStatus == RefreshStatus.IDLE) {
            if (topUpdateY == 0 && bottomUpdateY == 0) {
                Point mPoint = dragInfo.updatePoint;
                topUpdateY = Math.round(mPoint.position[1]);
                if (mPullDownRefreshEnable && topUpdateY > startY &&
                        topUpdateY < Math.round(Math.round(mDisplayPoint.position[1]) / 2)) {
                    addLoadingHeader();
                    bottomUpdateY = 0;
                } else if (topUpdateY < startY
                        && topUpdateY > Math.round(Math.round(mDisplayPoint.position[1]) / 2)) {
                    topUpdateY = 0;
                    bottomUpdateY = Math.round(mPoint.position[1]);
                } else {
                    LogUtil.debug("info", "Nothing");
                }
            }
        }
    }

    /**
     * initWholeHeaderView
     */
    private void initWholeHeaderView() {
        mWholeHeaderView = new DirectionalLayout(getContext().getApplicationContext());
        mWholeHeaderView.setLayoutConfig(new LayoutConfig(MATCH_PARENT, MATCH_CONTENT));
        mWholeHeaderView.setAlignment(CENTER);
        mWholeHeaderView.setOrientation(VERTICAL);
        addComponent(mWholeHeaderView);
    }

    /**
     * onFinishInflate
     *
     * @throws RuntimeException Exception Component
     */
    public void onFinishInflate() throws RuntimeException {
        if (getChildCount() < 1) {
            return;
        }
        mContentView = getComponentAt(0);
        LogUtil.error("onFinishInflate ", "" + getChildCount());
        mContentView = getComponentAt(1);
        LogUtil.error("onFinishInflate ", "" + mContentView.getClass().getSimpleName());
        if (mContentView instanceof ListContainer) {
            mRecyclerView = (ListContainer) mContentView;
        } else if (mContentView instanceof ScrollView) {
            mScrollView = (ScrollView) mContentView;
        } else {
            mNormalView = mContentView;
            mNormalView.setClickable(true);
        }
    }

    /**
     * refreshViewHolder
     *
     * @param refreshViewHolder RegreshViewHolder
     */
    public void setRefreshViewHolder(BgarefreshViewHolder refreshViewHolder) {
        mRefreshViewHolder = refreshViewHolder;
        mIsLoadingMore = refreshViewHolder.isLoadingMoreEnabled;
        mRefreshViewHolder.setRefreshLayout(this);
        initRefreshHeaderView();
        initLoadMoreFooterView();
    }

    /**
     * initRefreshHeaderView
     */
    private void initRefreshHeaderView() {
        if (mRefreshViewHolder != null) {
            myLayout = new DirectionalLayout(getContext().getApplicationContext());
            myLayout.setLayoutConfig(new LayoutConfig(MATCH_PARENT, MATCH_CONTENT));
            myLayout.setAlignment(CENTER);
            ShapeElement element = new ShapeElement();
            element.setRgbColor(RgbColor.fromArgbInt(Utils.getColor(getContext(),
                    mRefreshViewHolder.mRefreshViewBackgroundColorRes)));
            myLayout.setBackground(element);
            refreshRoundProgress = new RoundProgressBar(getContext().getApplicationContext());
            refreshRoundProgress.setLayoutConfig(new LayoutConfig(mRefreshViewHolder.progressSize,
                    mRefreshViewHolder.progressSize));
            refreshRoundProgress.setIndeterminate(true);
            logo = new Image(getContext());
            logo.setLayoutConfig(new LayoutConfig(mRefreshViewHolder.progressSize - 40,
                    mRefreshViewHolder.progressSize - 40));
            logo.setMarginLeft(20);
            logo.setMarginTop(20);
            StackLayout mStackLayout = new StackLayout(getContext());
            mStackLayout.setLayoutConfig(new LayoutConfig(MATCH_CONTENT, MATCH_CONTENT));
            mStackLayout.setMarginTop(8);
            mStackLayout.addComponent(logo);
            mStackLayout.addComponent(refreshRoundProgress);
            myLayout.addComponent(mStackLayout);
            logo.setVisibility(HIDE);
            refreshText = new Text(getContext().getApplicationContext());
            refreshText.setLayoutConfig(new LayoutConfig(MATCH_CONTENT, MATCH_CONTENT));
            refreshText.setText(mRefreshViewHolder.mPullDownRefreshText);
            refreshText.setTextColor(new Color(Utils.getColor(getContext(),
                    mRefreshViewHolder.loaderTextColor)));
            refreshText.setTextSize(mRefreshViewHolder.loaderTextSize);
            refreshText.setTextAlignment(TextAlignment.CENTER);
            refreshText.setMarginBottom(8);
            myLayout.addComponent(refreshText);
            mWholeHeaderView.addComponent(myLayout);
            myLayout.setVisibility(HIDE);
        }
    }

    /**
     * setCustomHeaderView
     *
     * @param customHeaderView Component
     * @param iscrollable      Boolean
     */
    public void setCustomHeaderView(Component customHeaderView, boolean iscrollable) {
        if (mCustomHeaderView != null && mCustomHeaderView.getComponentParent() != null) {
            ComponentParent componentParent = mCustomHeaderView.getComponentParent();
            componentParent.removeComponent(mCustomHeaderView);
        }
        mCustomHeaderView = customHeaderView;
        if (mCustomHeaderView != null) {
            mCustomHeaderView.setLayoutConfig(new LayoutConfig(MATCH_PARENT, MATCH_CONTENT));
            mWholeHeaderView.addComponent(mCustomHeaderView);
            mIsCustomHeaderViewScrollable = iscrollable;
        }
    }

    /**
     * initLoadMoreFooterView
     */
    private void initLoadMoreFooterView() {
        mLoadMoreFooterView = mRefreshViewHolder.getLoadMoreFooterView();
        if (mLoadMoreFooterView != null) {
            mLoadMoreFooterViewHeight = mLoadMoreFooterView.getHeight();
            mLoadMoreFooterView.setVisibility(HIDE);
        }
        onAttachedToWindow();
    }

    /**
     * onAttachedToWindow
     */
    protected void onAttachedToWindow() {
        if (!mIsInitedContentViewScrollListener && mLoadMoreFooterView != null) {
            addComponent(mLoadMoreFooterView, getChildCount());
            mIsInitedContentViewScrollListener = true;
        }
    }

    /**
     * shouldHandleRecyclerViewLoadingMore
     *
     * @param recyclerView ListContainer
     * @return false Boolean
     */
    private boolean shouldHandleRecyclerViewLoadingMore(ListContainer recyclerView) {
        if (mIsLoadingMore || mCurrentRefreshStatus == RefreshStatus.REFRESHING
                || mLoadMoreFooterView == null || bgarefreshLayoutDelegate == null
                || recyclerView.getItemProvider() == null || recyclerView.getItemProvider().getCount() == 0) {
            return false;
        }
        return BgarefreshScrollingUtil.isRecyclerViewToBottom(recyclerView);
    }

    /**
     * 结束下拉刷新
     */
    public void endRefreshing() {
        if (mCurrentRefreshStatus == RefreshStatus.REFRESHING
                || mCurrentRefreshStatus == RefreshStatus.RELEASE_REFRESH) {
            myLayout.setVisibility(HIDE);
            mCurrentRefreshStatus = RefreshStatus.IDLE;
            bottomUpdateY = 0;
            topUpdateY = 0;
            endY = 0;
        }
    }

    /**
     * beginLoadingMore
     */
    public void beginLoadingMore() {
        if (!mIsLoadingMore && mLoadMoreFooterView != null && bgarefreshLayoutDelegate != null
                && bgarefreshLayoutDelegate.onBgarefreshLayoutBeginLoadingMore(this)) {
            mIsLoadingMore = true;
            if (mIsShowLoadingMoreView) {
                showLoadingMoreView();
            }
        }
        showPopupDialog();
    }

    /**
     * setPullDownRefreshEnable
     *
     * @param pullDownRefreshEnable boolean
     */
    public void setPullDownRefreshEnable(boolean pullDownRefreshEnable) {
        mPullDownRefreshEnable = pullDownRefreshEnable;
    }

    /**
     * setIsShowLoadingMoreView
     *
     * @param isShowLoadingMoreView boolean
     */
    public void setIsShowLoadingMoreView(boolean isShowLoadingMoreView) {
        mIsShowLoadingMoreView = isShowLoadingMoreView;
    }

    /**
     * isLoadingMore
     *
     * @return boolean
     */
    public boolean isLoadingMore() {
        return mIsLoadingMore;
    }

    /**
     * showLoadingMoreView
     */
    private void showLoadingMoreView() {
        mRefreshViewHolder.changeToLoadingMore();
        mLoadMoreFooterView.setVisibility(VISIBLE);
        BgarefreshScrollingUtil.scrollToBottom(mScrollView);
    }

    /**
     * endLoadingMore
     */
    public void endLoadingMore() {
        mEventHandler.postTask(mDelayHiddenLoadingMoreViewTask, Constants.CLOSE_TIME_800);
    }

    /**
     * mDelayHiddenLoadingMoreViewTask
     */
    private Runnable mDelayHiddenLoadingMoreViewTask = new Runnable() {
        @Override
        public void run() {
            if (mLoadMoreFooterView != null) {
                mLoadMoreFooterView.setVisibility(HIDE);
                endRefreshing();
            }
            endRefreshing();
            hidePopupDialog();
        }
    };

    /**
     * setDelegate
     *
     * @param delegate BgarefreshLayoutDelegate
     */
    public void setDelegate(BgarefreshLayoutDelegate delegate) {
        bgarefreshLayoutDelegate = delegate;
        onFinishInflate();
    }

    /**
     * getCurrentRefreshStatus
     *
     * @return RefreshStatus
     */
    public RefreshStatus getCurrentRefreshStatus() {
        return mCurrentRefreshStatus;
    }

    /**
     * 切换到正在刷新状态，会调用delegate的onBGARefreshLayoutBeginRefreshing方法
     */
    public void beginRefreshing() {
        if (mCurrentRefreshStatus != RefreshStatus.REFRESHING && bgarefreshLayoutDelegate != null) {
            if (bgarefreshLayoutDelegate != null) {
                bgarefreshLayoutDelegate.onBgarefreshLayoutBeginRefreshing(this);
            }
            addLoadingHeader();
            mCurrentRefreshStatus = RefreshStatus.REFRESHING;
        }
    }

    /**
     * Function description
     * BgarefreshScaleDelegate interface
     */
    public interface BgarefreshLayoutDelegate {
        /**
         * onBgarefreshLayoutBeginRefreshing
         *
         * @param refreshLayout BgarefreshLayout
         */
        void onBgarefreshLayoutBeginRefreshing(BgarefreshLayout refreshLayout);

        /**
         * onBGARefreshLayoutBeginLoadingMore
         *
         * @param refreshLayout BgarefreshLayout
         * @return true Boolean
         */
        boolean onBgarefreshLayoutBeginLoadingMore(BgarefreshLayout refreshLayout);
    }

    /**
     * Function description
     * BgarefreshScaleDelegate interface
     */
    public interface BgarefreshScaleDelegate {
        /**
         * onRefreshScaleChanged
         *
         * @param moveYdistance MovieYDistance
         * @param scale         Scale
         */
        void onRefreshScaleChanged(float scale, int moveYdistance);
    }

    /**
     * Function description
     * RefreshStatus enum Constants
     */
    public enum RefreshStatus {
        /**
         * IDEl
         */
        IDLE,

        /**
         * RELEASE_REFRESH
         */
        RELEASE_REFRESH,

        /**
         * REFRESHING
         */
        REFRESHING
    }

    /**
     * dp2px
     *
     * @param context Context
     * @param dpValue DapValue
     * @return true Boolean
     */
    public static int dp2px(Context context, int dpValue) {
        return dpValue;
    }

    @Override
    public boolean onDrag(Component component, DragEvent event) {
        if (event.getAction() == DRAG_UP) {
            LogUtil.error(mCount + " onDrag ", "" + DRAG_UP);
        }
        if (event.getAction() == DRAG_DOWN) {
            LogUtil.error(mCount + " onDrag ", "" + DRAG_DOWN);
        }
        return super.onDrag(component, event);
    }


    private void addLoadingHeader() {
        if (mCurrentRefreshStatus == RefreshStatus.IDLE) {
            myLayout.setVisibility(VISIBLE);
            mRefreshViewHolder.refreshHeaderView = mWholeHeaderView;
            if (mRefreshViewHolder.mRefreshViewBackgroundDrawableRes == -1) {
                ShapeElement element = new ShapeElement();
                element.setRgbColor(RgbColor.fromArgbInt(Utils.getColor(getContext(),
                        mRefreshViewHolder.mRefreshViewBackgroundColorRes)));
                myLayout.setBackground(element);
            } else {
                myLayout.setBackground(Utils.getElementByResId(getContext(),
                        mRefreshViewHolder.mRefreshViewBackgroundDrawableRes).get());
            }
            refreshText.setText(mRefreshViewHolder.mPullDownRefreshText);
            refreshText.setTextSize(mRefreshViewHolder.loaderTextSize);
            if (mRefreshViewHolder.loaderTextColor != -1) {
                refreshText.setTextColor(new Color(Utils.getColor(getContext(),
                        mRefreshViewHolder.loaderTextColor)));
            } else {
                refreshText.setTextColor(Color.GRAY);
            }
            refreshRoundProgress.setProgressColor(new Color(Utils.getColor(getContext(),
                    mRefreshViewHolder.mUltimateColorResId)));
            if (mRefreshViewHolder.mOriginalImageResId != -1) {
                logo.setImageElement(Utils.getElementByResId(getContext(),
                        mRefreshViewHolder.mOriginalImageResId).get());
                logo.setVisibility(VISIBLE);
            }
        }
    }

    private void hidePopupDialog() {
        if (popupDialog != null && popupDialog.isShowing()) {
            popupDialog.hide();
        }
        if (mIsShowLoadingMoreView && myBottomParentLayout != null) {
            myBottomParentLayout.setVisibility(HIDE);
            if (getComponentAt(1) instanceof ListContainer) {
                ListContainer tempList = (ListContainer) getComponentAt(1);
                tempList.setMarginBottom(0);
                if (listSize != -1) {
                    tempList.scrollTo(listSize);
                    listSize = -1;
                }
            }
        }
    }

    private void showPopupDialog() {
        popupDialog = new CommonDialog(getContext());
        Component dialogComponent = LayoutScatter.getInstance(getContext())
                .parse(ResourceTable.Layout_dialog_loader, null, false);
        popupDialog.setSwipeToDismiss(false);
        RoundProgressBar roundProgressBar = null;
        if (dialogComponent.findComponentById(
                ResourceTable.Id_roundProgressBar) instanceof RoundProgressBar) {
            roundProgressBar = (RoundProgressBar) dialogComponent.findComponentById(
                    ResourceTable.Id_roundProgressBar);
            roundProgressBar.setIndeterminate(true);
            roundProgressBar.setLayoutConfig(new LayoutConfig(mRefreshViewHolder.progressSize,
                    mRefreshViewHolder.progressSize));
            roundProgressBar.setProgressColor(new Color(Utils.getColor(getContext(),
                    mRefreshViewHolder.mUltimateColorResId)));
            DirectionalLayout parentLayout = null;
            if (dialogComponent.findComponentById(ResourceTable.Id_parentLayout) instanceof DirectionalLayout) {
                parentLayout = (DirectionalLayout) dialogComponent.findComponentById(ResourceTable.Id_parentLayout);
            }
            ShapeElement element = new ShapeElement();
            element.setRgbColor(RgbColor.fromArgbInt(Utils.getColor(getContext(),
                    mRefreshViewHolder.mLoadMoreBackgroundColorRes)));
            parentLayout.setBackground(element);
            if (dialogComponent.findComponentById(ResourceTable.Id_text) instanceof Text) {
                Text text = (Text) dialogComponent.findComponentById(ResourceTable.Id_text);
                text.setText(mRefreshViewHolder.loadMoreText);
                text.setTextSize(mRefreshViewHolder.loaderTextSize);

                if (mRefreshViewHolder.loaderTextColor != -1) {
                    text.setTextColor(new Color(Utils.getColor(getContext(),
                            mRefreshViewHolder.loaderTextColor)));
                } else {
                    text.setTextColor(Color.GRAY);
                }
            }
        }
        popupDialog.setContentCustomComponent(dialogComponent);
        if (mRefreshViewHolder.pullUpDilogueVisible) {
            popupDialog.show();
        }
        if (mIsShowLoadingMoreView) {
            bottomView();
        }
    }

    private void bottomView() {
        myBottomParentLayout = new DirectionalLayout(getContext().getApplicationContext());
        myBottomParentLayout.setLayoutConfig(new LayoutConfig(MATCH_PARENT, MATCH_CONTENT));
        myBottomParentLayout.setAlignment(BOTTOM);
        myBottomLayout = new DirectionalLayout(getContext().getApplicationContext());
        myBottomLayout.setLayoutConfig(new LayoutConfig(MATCH_PARENT, MATCH_CONTENT));
        myBottomLayout.setAlignment(CENTER);
        myBottomLayout.setOrientation(HORIZONTAL);
        if (mRefreshViewHolder.mLoadMoreBackgroundDrawableRes == -1) {
            ShapeElement element = new ShapeElement();
            element.setRgbColor(RgbColor.fromArgbInt(Utils.getColor(getContext(),
                    mRefreshViewHolder.mLoadMoreBackgroundColorRes)));
            myBottomLayout.setBackground(element);
        } else {
            myBottomLayout.setBackground(Utils.getElementByResId(getContext(),
                    mRefreshViewHolder.mLoadMoreBackgroundDrawableRes).get());
        }
        RoundProgressBar refreshBottomRoundProgress = new RoundProgressBar(getContext().getApplicationContext());
        refreshBottomRoundProgress.setLayoutConfig(new LayoutConfig(mRefreshViewHolder.progressSize,
                mRefreshViewHolder.progressSize));
        refreshBottomRoundProgress.setIndeterminate(true);
        refreshBottomRoundProgress.setProgressColor(new Color(Utils.getColor(getContext(),
                mRefreshViewHolder.mUltimateColorResId)));
        refreshBottomRoundProgress.setMarginTop(3);
        refreshBottomRoundProgress.setMarginBottom(3);
        myBottomLayout.addComponent(refreshBottomRoundProgress);
        Text refreshBottomText = new Text(getContext().getApplicationContext());
        refreshBottomText.setLayoutConfig(new LayoutConfig(MATCH_CONTENT, MATCH_CONTENT));
        refreshBottomText.setText(mRefreshViewHolder.loadMoreText);
        if (mRefreshViewHolder.loaderTextColor != -1) {
            refreshBottomText.setTextColor(new Color(Utils.getColor(getContext(),
                    mRefreshViewHolder.loaderTextColor)));
        } else {
            refreshBottomText.setTextColor(Color.GRAY);
        }
        refreshBottomText.setTextAlignment(TextAlignment.CENTER);
        refreshBottomText.setMarginLeft(8);
        refreshBottomText.setTextSize(mRefreshViewHolder.loaderTextSize);
        myBottomLayout.addComponent(refreshBottomText);
        myBottomParentLayout.addComponent(myBottomLayout);
        addComponent(myBottomParentLayout);
        mRefreshViewHolder.mLoadMoreFooterView = myBottomLayout;
        if (getComponentAt(1) instanceof ListContainer) {
            ListContainer tempList = (ListContainer) getComponentAt(1);
            listSize = tempList.getChildCount() - 1;
            if (listSize != -1) {
                tempList.scrollTo(listSize);
            }
        }
    }
}