/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.bingoogolapple.refreshlayoutsample.provier;

import cn.bingoogolapple.refreshlayoutsample.ResourceTable;
import cn.bingoogolapple.refreshlayoutsample.slice.ListSlice;
import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Text;
import ohos.agp.components.LayoutScatter;

import java.util.List;

/**
 * Function description
 * ContactItemProvider implement BaseItemProvider
 */
public class ContactItemProvider extends BaseItemProvider {
    List<String> mList;
    ListSlice mListSlice;

    /**
     * ContactItemProvider name
     *
     * @param listitems ListItem
     * @param listSlice listSlice
     */
    public ContactItemProvider(List<String> listitems, ListSlice listSlice) {
        this.mList = listitems;
        this.mListSlice = listSlice;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    /**
     * addIem
     *
     * @param item new item
     */
    public void addIem(String item) {
        mList.add(item);
        notifyDataChanged();
    }

    /**
     * getItem
     *
     * @param position Position
     * @return Object object
     */
    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    /**
     * getItem
     *
     * @param position Position
     * @return long Value
     */
    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * getItem
     *
     * @param position           Position
     * @param component          Component
     * @param componentContainer Container
     * @return long Value
     */
    @Override
    public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
        Text text = null;
        Component rootView = LayoutScatter.getInstance(mListSlice)
                .parse(ResourceTable.Layout_provider_list, null, false);
        if (rootView.findComponentById(ResourceTable.Id_test) instanceof Text) {
            text = (Text) rootView.findComponentById(ResourceTable.Id_test);
        }
        text.setText(position + " " + mList.get(position));
        return rootView;
    }
}
