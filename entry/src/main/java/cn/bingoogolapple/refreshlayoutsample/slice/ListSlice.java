/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.bingoogolapple.refreshlayoutsample.slice;

import cn.bingoogolapple.refreshlayout.BgaStickinessRefreshViewHolder;
import cn.bingoogolapple.refreshlayout.BgarefreshLayout;
import cn.bingoogolapple.refreshlayoutsample.ResourceTable;
import cn.bingoogolapple.refreshlayoutsample.provier.ContactItemProvider;
import ohos.aafwk.content.Intent;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.ListContainer;

import java.util.ArrayList;

/**
 * Function description
 * ListSlice extend MainAbilitySlice
 */
public class ListSlice extends MainAbilitySlice implements BgarefreshLayout.BgarefreshLayoutDelegate {
    ListContainer listContainer;
    private ComponentContainer componentContainer;
    private BgarefreshLayout bgarefreshLayout;
    private ContactItemProvider contactItemProvider;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        if (LayoutScatter.getInstance(this)
                .parse(ResourceTable.Layout_slice_list, null, false) instanceof ComponentContainer) {
            componentContainer = (ComponentContainer) LayoutScatter.getInstance(this)
                    .parse(ResourceTable.Layout_slice_list, null, false);
        }
        if (componentContainer.findComponentById(ResourceTable.Id_mList) instanceof ListContainer) {
            listContainer = (ListContainer) componentContainer.findComponentById(ResourceTable.Id_mList);
        }
        if (componentContainer.findComponentById(ResourceTable.Id_bgaRefreshLayout) instanceof BgarefreshLayout) {
            bgarefreshLayout = (BgarefreshLayout)
                    componentContainer.findComponentById(ResourceTable.Id_bgaRefreshLayout);
        }

        super.setUIContent(componentContainer);
        bgarefreshLayout.setDelegate(this);

        bgarefreshLayout.setIsShowLoadingMoreView(true);
        bgarefreshLayout.setPullDownRefreshEnable(true);

        BgaStickinessRefreshViewHolder stickinessRefreshViewHolder = new BgaStickinessRefreshViewHolder(this,
                true);

        stickinessRefreshViewHolder.setPullDownRefreshText("Company Calling..");
        stickinessRefreshViewHolder.setLoadingMoreText("Loading Pull..");
        stickinessRefreshViewHolder.setUltimateColor(ResourceTable.Color_util__color);
        stickinessRefreshViewHolder.setLoadMoreBackgroundColorRes(ResourceTable.Color_load_more);
        stickinessRefreshViewHolder.setRefreshViewBackgroundColorRes(ResourceTable.Color_refresh_color);
        stickinessRefreshViewHolder.setOriginalImage(ResourceTable.Media_custom_mooc_icon);
        stickinessRefreshViewHolder.setLoaderTextColor(ResourceTable.Color_text_color);
        stickinessRefreshViewHolder.setPullUpDilogueVisible(false);
        stickinessRefreshViewHolder.setLoaderTextSize(50);
        stickinessRefreshViewHolder.setProgressSize(90);
        bgarefreshLayout.setRefreshViewHolder(stickinessRefreshViewHolder);
    }

    @Override
    public void onActive() {
        super.onActive();
        listCountForScroll();
    }

    /**
     * listContainerAddedClass name
     */
    public void listCountForScroll() {
        ArrayList<String> mArray = new ArrayList<>();
        mArray.add("hello1");
        mArray.add("hello2");
        mArray.add("hello3");
        mArray.add("hello4");
        mArray.add("hello5");
        mArray.add("hello6");
        mArray.add("hello7");
        mArray.add("hello8");
        mArray.add("hello9");
        mArray.add("hello10");
        mArray.add("hello11");
        mArray.add("hello12");
        mArray.add("hello13");
        mArray.add("hello14");
        mArray.add("hello23");
        mArray.add("hello24");
        mArray.add("hello25");
        mArray.add("hello26");
        mArray.add("hello27");
        mArray.add("hello28");
        mArray.add("hello29");
        mArray.add("hello30");
        mArray.add("hello Last");
        contactItemProvider = new ContactItemProvider(mArray, this);
        listContainer.setItemProvider(contactItemProvider);
    }

    @Override
    public void onBgarefreshLayoutBeginRefreshing(BgarefreshLayout refreshLayout) {
    }

    @Override
    public boolean onBgarefreshLayoutBeginLoadingMore(BgarefreshLayout refreshLayout) {
        return false;
    }
}
