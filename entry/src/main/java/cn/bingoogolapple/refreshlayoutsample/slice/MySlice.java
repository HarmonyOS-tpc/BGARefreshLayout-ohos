/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.bingoogolapple.refreshlayoutsample.slice;

import cn.bingoogolapple.refreshlayout.BgaStickinessRefreshViewHolder;
import cn.bingoogolapple.refreshlayout.BgarefreshLayout;
import cn.bingoogolapple.refreshlayoutsample.ResourceTable;
import cn.bingoogolapple.refreshlayoutsample.utils.Constants;
import ohos.aafwk.content.Intent;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;

/**
 * MySlice.
 */
public class MySlice extends MainAbilitySlice implements BgarefreshLayout.BgarefreshLayoutDelegate {
    private static final String CUSTOM_REFRESH_TEXT = "Company Calling..";
    private static final String CUSTOM_PULL_DOWN_TEXT = "Loading Pull..";
    private ComponentContainer componentContainer;
    private BgarefreshLayout bgarefreshLayout;
    private Text text;
    private int counter = 1;

    /**
     * cube for testing
     *
     * @param index index
     * @return value log message
     */
    public static int cube(int index) {
        int value = index * index * index;
        return value;
    }

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        if (LayoutScatter.getInstance(this)
                .parse(ResourceTable.Layout_ability_main, null, false) instanceof ComponentContainer) {
            componentContainer = (ComponentContainer) LayoutScatter.getInstance(this)
                    .parse(ResourceTable.Layout_ability_main, null, false);
        }

        super.setUIContent(componentContainer);
        if (componentContainer.findComponentById(ResourceTable.Id_mainLayout) instanceof BgarefreshLayout) {
            bgarefreshLayout = (BgarefreshLayout) componentContainer.findComponentById(ResourceTable.Id_mainLayout);
        }
        if (componentContainer.findComponentById(ResourceTable.Id_message) instanceof Text) {
            text = (Text) componentContainer.findComponentById(ResourceTable.Id_message);
        }
        text.setText("Hello MySlice Init");
        bgarefreshLayout.setDelegate(this);
        BgaStickinessRefreshViewHolder stickinessRefreshViewHolder = new BgaStickinessRefreshViewHolder(this,
                true);
        stickinessRefreshViewHolder.setPullDownRefreshText(CUSTOM_REFRESH_TEXT);
        stickinessRefreshViewHolder.setLoadingMoreText(CUSTOM_PULL_DOWN_TEXT);
        stickinessRefreshViewHolder.setUltimateColor(ResourceTable.Color_util__color);
        stickinessRefreshViewHolder.setLoadMoreBackgroundColorRes(ResourceTable.Color_load_more);
        stickinessRefreshViewHolder.setRefreshViewBackgroundColorRes(ResourceTable.Color_refresh_color);
        stickinessRefreshViewHolder.setOriginalImage(ResourceTable.Media_custom_mooc_icon);
        stickinessRefreshViewHolder.setPullUpDilogueVisible(false);

        bgarefreshLayout.setRefreshViewHolder(stickinessRefreshViewHolder);
        text.setText(text.getText() + Constants.NEXT_LINE + "Delegate");
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onBgarefreshLayoutBeginRefreshing(BgarefreshLayout refreshLayout) {
        text.setText(counter + " Swipe count");
        counter++;
    }

    @Override
    public boolean onBgarefreshLayoutBeginLoadingMore(BgarefreshLayout refreshLayout) {
        text.setText(counter + " Called Loading");
        counter++;
        return false;
    }
}