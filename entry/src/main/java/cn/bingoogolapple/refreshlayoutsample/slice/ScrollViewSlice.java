/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.bingoogolapple.refreshlayoutsample.slice;

import cn.bingoogolapple.refreshlayout.BgarefreshLayout;
import cn.bingoogolapple.refreshlayout.BgaStickinessRefreshViewHolder;
import cn.bingoogolapple.refreshlayoutsample.ResourceTable;
import cn.bingoogolapple.refreshlayoutsample.utils.Constants;
import ohos.aafwk.content.Intent;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.ScrollView;
import ohos.agp.components.Text;
import ohos.agp.window.dialog.ToastDialog;

/**
 * Function description
 * class Constants
 */
public class ScrollViewSlice extends MainAbilitySlice implements BgarefreshLayout.BgarefreshLayoutDelegate {
    private ComponentContainer rootContainer;
    private Text text;
    private ScrollView scrollView;
    private int counter = 1;
    private BgarefreshLayout bgarefreshLayout;
    private String textString = "Hello ScrollViewSlice Init";

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        if (LayoutScatter.getInstance(this)
                .parse(ResourceTable.Layout_slice_scroll_view, null, false)
                instanceof ComponentContainer) {
            rootContainer = (ComponentContainer) LayoutScatter.getInstance(this)
                    .parse(ResourceTable.Layout_slice_scroll_view, null, false);
        }
        super.setUIContent(rootContainer);
        if (rootContainer.findComponentById(ResourceTable.Id_mainLayout) instanceof BgarefreshLayout) {
            bgarefreshLayout = (BgarefreshLayout) rootContainer.findComponentById(ResourceTable.Id_mainLayout);
        }
        if (rootContainer.findComponentById(ResourceTable.Id_message) instanceof Text) {
            text = (Text) rootContainer.findComponentById(ResourceTable.Id_message);
        }
        if (rootContainer.findComponentById(ResourceTable.Id_scrollView) instanceof ScrollView) {
            scrollView = (ScrollView) rootContainer.findComponentById(ResourceTable.Id_scrollView);
        }
        text.setText(textString);
        bgarefreshLayout.setDelegate(this);
        BgaStickinessRefreshViewHolder stickinessRefreshViewHolder = new BgaStickinessRefreshViewHolder(this,
                true);
        bgarefreshLayout.setRefreshViewHolder(stickinessRefreshViewHolder);
        text.setText(text.getText() + Constants.NEXT_LINE + "Delegate");
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onBgarefreshLayoutBeginRefreshing(BgarefreshLayout refreshLayout) {
        new ToastDialog(getContext()).setText("List Refresh").show();
    }

    @Override
    public boolean onBgarefreshLayoutBeginLoadingMore(BgarefreshLayout refreshLayout) {
        new ToastDialog(getContext()).setText("List Loading More").show();
        return false;
    }
}
