/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.bingoogolapple.refreshlayoutsample.utils;

/**
 * Function description
 * class Constants
 *
 */
public class Constants {
    /**
     * BUNDLE_NAME
     */
    public static final String BUNDLE_NAME = "cn.bingoogolapple.refreshlayoutsample";

    /**
     * TAG_VAL
     */
    public static final String TAG_VAL = "IOEXCEPTION";

    /**
     * BUFFER_OFFSET_ID_CONSTANT
     */
    public static final int BUFFER_OFFSET_ID_CONSTANT = 0x4;

    /**
     * BUFFER_OFFSET_ID_CONSTANT
     */
    public static final int BUFFER_OFFSET_ID_DETAILS_CONSTANT = 0x8;

    /**
     * BUFFER_OFFSET_LONG_ID_CONSTANT
     */
    public static final int BUFFER_OFFSET_LONG_ID_CONSTANT = 8;

    /**
     * BUFFER_OFFSET_LONG_ID_DETAILS_CONSTANT
     */
    public static final int BUFFER_OFFSET_LONG_ID_DETAILS_CONSTANT = 16;

    /**
     * Request ID for STORAGE_PERMISSION_REQ_ID
     */
    public static final int STORAGE_PERMISSION_REQ_ID = 1000;

    /**
     * Invalid ID
     */
    public static final int INVALID = -1;

    /**
     * MAGIC_2 ID
     */
    public static final int MAGIC_2 = 2;

    /**
     * MAGIC_255 ID
     */
    public static final int MAGIC_255 = 255;

    /**
     * MAGIC_200 ID
     */
    public static final int MAGIC_32 = 32;

    /**
     * MAGIC_30 ID
     */
    public static final int MAGIC_30 = 30;

    /**
     * RGB_COLOR ID
     */
    public static final int RGB_COLOR = 0xFF000000;

    /**
     * MAGIC_10 ID
     */
    public static final String NEXT_LINE = "\n\n";

    /**
     * MAGIC_50 ID
     */
    public static final int MAGIC_50 = 50;

    private Constants() {
        /* Do nothing */
    }
}
