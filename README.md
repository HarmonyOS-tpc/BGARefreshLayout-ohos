# BGARefreshLayout-ohos

Developers can use BGARefreshLayout-ohos to implement multiple pull-down refresh effects, pull-ups to load more.

# BGARefreshLayout-ohos includes :
   	* Provides Swipe Up and Swipe Down capabilities.
   	* Support Swipe Down show Loader on Top and call back accrue in used Slice
  	 * Support Swipe Up and Show PopUp Loader and call back accrue in Used Slice

Note:-  Put weight 1 in sub component
    Like :-         ohos:weight="1"

# Usage Instructions
 1.   Configure BGARefreshLayout in Ability or AbilitySlice


        private BGARefreshLayout bgaRefreshLayout;
        bgaRefreshLayout = (BGARefreshLayout) rootContainer.findComponentById(ResourceTable.Id_mainLayout);
        bgaRefreshLayout.setDelegate(this);
        BGAStickinessRefreshViewHolder stickinessRefreshViewHolder = new BGAStickinessRefreshViewHolder(this,
                false);
        bgaRefreshLayout.setRefreshViewHolder(stickinessRefreshViewHolder);
        bgarefreshLayout.setIsShowLoadingMoreView(true);
        bgarefreshLayout.setPullDownRefreshEnable(true);
        stickinessRefreshViewHolder.setPullDownRefreshText("Custom Text....");
        stickinessRefreshViewHolder.setLoadingMoreText("Custom Text....");
        stickinessRefreshViewHolder.setUltimateColor(ResourceTable.Color_util__color);
        stickinessRefreshViewHolder.setLoadMoreBackgroundColorRes(ResourceTable.Color_load_more);
        stickinessRefreshViewHolder.setRefreshViewBackgroundColorRes(ResourceTable.Color_refresh_color);
        stickinessRefreshViewHolder.setOriginalImage(ResourceTable.Media_custom_mooc_icon);
        stickinessRefreshViewHolder.setLoaderTextColor(ResourceTable.Color_text_color);
        stickinessRefreshViewHolder.setPullUpDilogueVisible(false);
        stickinessRefreshViewHolder.setLoaderTextSize(50);
        stickinessRefreshViewHolder.setProgressSize(90);


User perform Action swipe-Up and Load-More  retun callbackmethod

         @Override
          public void onBGARefreshLayoutBeginRefreshing(BGARefreshLayout refreshLayout) {
                        mTextView.setText(counter + " Called Swipe");
                        counter++;
          }

          @Override
          public boolean onBGARefreshLayoutBeginLoadingMore(BGARefreshLayout refreshLayout) {
                        mTextView.setText(counter + " Called Loading");
                        counter++;
                        return false;
          }

2 . For the direct child control of BGARefreshLayout, please use android:layout_weight="1"

    	<?xml version="1.0" encoding="utf-8"?>
          <cn.bingoogolapple.refreshlayout.BgarefreshLayout
              xmlns:ohos="http://schemas.huawei.com/res/ohos"
              ohos:id="$+id:bgaRefreshLayout"
              ohos:height="match_parent"
              ohos:width="match_parent"
              ohos:orientation="vertical">

              <ListContainer
                  ohos:id="$+id:mList"
                  ohos:height="match_parent"
                  ohos:width="match_parent"
                  ohos:weight="1"
                  />

          </cn.bingoogolapple.refreshlayout.BgarefreshLayout>


# Installation tutorial

1.Using the library.har, make sure to add library.har file in the entry/libs folder.
   Modify entry build.gradle as below :

           dependencies {
                implementation fileTree(dir: 'libs', include: [' *.jar', ' *.har'])
           }


2.For using library module in sample app, Add the dependencies in entry/build.gradle as below
   Modify entry build.gradle as below :

           dependencies {
               implementation project(path: ':library')
           }


3.For using BGARefreshLayout-ohos from a remote repository in separate application, add the below dependencies :
   Modify entry build.gradle as below :

    	dependencies {
    	    implementation 'io.openharmony.tpc.thirdlib:BGARefreshLayout-ohos:1.0.0'
    	}

# License

    Copyright 2015 bingoogolapple

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.